import cv2
import matplotlib.pyplot as plt
import numpy as np


def averaging_same_size(image, x_blocks, y_blocks):
    image = image.copy()
    x_size = image.shape[0] // x_blocks
    y_size = image.shape[1] // y_blocks

    for i in range(x_blocks):
        for j in range(y_blocks):
            if i == x_blocks - 1 and j == y_blocks:
                image[i * x_size:, j * y_size:] = \
                    np.mean(image[i * x_size:, j * y_size:])
            elif i == x_blocks:
                image[i * x_size:, j * y_size: (j + 1) * y_size] = \
                    np.mean(image[i * x_size:, j * y_size: (j + 1) * y_size])
            elif j == y_blocks:
                image[i * x_size: (i + 1) * x_size, j * y_size:] = \
                    np.mean(image[i * x_size: (i + 1) * x_size, j * y_size:])
            else:
                image[i * x_size: (i + 1) * x_size, j * y_size: (j + 1) * y_size] = \
                    np.mean(image[i * x_size: (i + 1) * x_size, j * y_size: (j + 1) * y_size])
    return image


def averaging_resize(image, x_blocks, y_blocks):
    image = image.copy()
    add_x = image.shape[0] // x_blocks - image.shape[0] % x_blocks
    add_y = image.shape[1] // y_blocks - image.shape[1] % y_blocks
    new_img = np.zeros((image.shape[0] + add_x, image.shape[1] + add_y))
    new_img[:image.shape[0], :image.shape[1]] = image[:image.shape[0], :image.shape[1]]
    image = new_img
    x_size = image.shape[0] // x_blocks
    y_size = image.shape[1] // y_blocks
    for i in range(x_blocks):
        for j in range(y_blocks):
            image[i * x_size: (i + 1) * x_size, j * y_size: (j + 1) * y_size] = \
                np.mean(image[i * x_size: (i + 1) * x_size, j * y_size: (j + 1) * y_size])

    return image


img = cv2.imread("cat.jpg")
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

new_gray1 = averaging_same_size(gray, 50, 50)
new_gray2 = averaging_resize(gray, 50, 50)

plt.suptitle("Block averaging 50x50")
plt.subplot(1, 3, 1)
plt.title("Original")
plt.imshow(gray, cmap="gray")
plt.subplot(1, 3, 2)
plt.title("W/o resizing")
plt.imshow(new_gray1, cmap="gray")
plt.subplot(1, 3, 3)
plt.title("With resizing")
plt.imshow(new_gray2, cmap="gray")

plt.show()
